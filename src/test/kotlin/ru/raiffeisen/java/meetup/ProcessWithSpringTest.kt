package ru.raiffeisen.java.meetup

import org.camunda.bpm.engine.test.Deployment
import org.camunda.bpm.engine.test.ProcessEngineRule
import org.camunda.bpm.engine.test.assertions.ProcessEngineTests
import org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest

@RunWith(CamundaCoverageSpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [ProcessEngineRuleConfiguration::class, HelloDelegate::class])
@Deployment(resources = ["processes/example.bpmn"])
class ProcessWithSpringTest {

    @Test
    fun subtractionTest() {
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ExampleProcess", "12345",
            mapOf(Pair("operation", "sub"), Pair("variableOne", 2), Pair("variableTwo", 1))
        )
        assertThat(processInstance).isWaitingAt("SubtractionTask")
        execute(job())
        assertThat(processInstance).isWaitingAt("ShowTask")
        complete(task(processInstance))
        assertThat(processInstance).isEnded
        val result = rule.historyService
            .createHistoricVariableInstanceQuery()
            .processInstanceId(processInstance.id)
            .variableName("variableResult")
            .singleResult()
        assertNotNull(result)
        assertEquals(1L, result.value)
    }

    @Test
    fun additionTest() {
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ExampleProcess", "12345",
            mapOf(Pair("operation", "add"), Pair("variableOne", 2), Pair("variableTwo", 1))
        )
        assertThat(processInstance).isWaitingAt("AdditionTask")
        execute(job())
        assertThat(processInstance).isWaitingAt("ShowTask")
        complete(task(processInstance))
        assertThat(processInstance).isEnded
        val result = rule.historyService
            .createHistoricVariableInstanceQuery()
            .processInstanceId(processInstance.id)
            .variableName("variableResult")
            .singleResult()
        assertNotNull(result)
        assertEquals(3L, result.value)
    }

    @Test
    fun errorTest() {
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ExampleProcess", "12345",
            mapOf(Pair("operation", "error"))
        )
        assertThat(processInstance).isEnded
        val result = rule.historyService
            .createHistoricActivityInstanceQuery()
            .processInstanceId(processInstance.id)
            .activityName("Error").singleResult()
        assertNotNull(result)
    }

    @Test
    fun startOnMiddleTaskTest() {
        val processInstance = rule.runtimeService.createProcessInstanceByKey("ExampleProcess")
            .startBeforeActivity("AdditionTask")
            .setVariables(mapOf(Pair("variableOne", 2), Pair("variableTwo", 1)))
            .execute()
        assertThat(processInstance).isWaitingAt("AdditionTask")
        execute(ProcessEngineTests.job("AdditionTask", processInstance))
        assertThat(processInstance).isWaitingAt("ShowTask")
        complete(task(processInstance))
        assertThat(processInstance).isEnded
    }

    companion object {
        @Rule
        @ClassRule
        @JvmStatic
        lateinit var rule: ProcessEngineRule
    }

}